function funcionesCalc()
{
	var cadena1=(document.getElementById('num1').value);
	var cadena2=(document.getElementById('num2').value);
	if (cadena1 == "" || cadena2 == "")
	{
		alert ("Debe ingresar los datos solicitados");
	}
	else
	{
		var operacion=(document.getElementById('operacion').value);
		var numero1=parseInt(document.getElementById('num1').value);
		var numero2=parseInt(document.getElementById('num2').value);
		var resultado;
		
		switch(operacion)
		{
			case "sumar":
				resultado = numero1 + numero2;
				alert ("El resultado de la suma es: "+resultado);
				break;
			case "restar":
				resultado = numero1 - numero2;
				alert ("El resultado de la resta es: "+resultado);
				break;
			case "multiplicar":
				resultado = numero1 * numero2;
				alert ("El resultado de la multiplicacion es: "+resultado);
				break;
			case "dividir":
				if (numero2!=0)
				{
					resultado = numero1 / numero2;
					alert ("El resultado de la division es: "+resultado);
				}
				else
				{
					alert ("Al dividir, el segundo número debe ser distinto de cero");
				}
				break;
			default:
				alert ("Debe seleccionar una operacón");
				break;
		}
	}
}