﻿<?php
session_start();

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Calculadora en PHP</title>    
  </head>
  
  <br>
  <body>
  
	<table width="60%" align=center border="1">
	<tr>
    <th scope="col">
	<center>
		<h1>Calculadora</h1>
		<h3>Ingrese los datos solicitados</h3>
	
		<form id="calc" name="calc" action="procesoCalc.php" method="post">

		<p>
		Número 1:
		<input  name="num1" type="text"  id="num1" />

		<?php

		if(isset($_SESSION['error']))
		{
			echo "<font color='red'>". $_SESSION['error']."</font>";
		}
		else
		{
			echo "";
		}
		unset($_SESSION['error']);
		?>

		<br><br>
		Número 2:
		<input name="num2" type="text" id="num2"  />

		<?php

		if(isset($_SESSION['error1']))
		{
			echo "<font color='red'>". $_SESSION['error1']."</font>";
		}
		else
		{
			echo "";
		}
		unset($_SESSION['error1']);
		?>

		<br><br>
		Operación a realizar:
		<select name="operacion" id="operacion" title="Operación"> 
			  <option>Seleccione una operación</option>
			  <option value="sumar">Sumar</option>
			  <option value="restar">Restar</option>
			  <option value="multiplicar">Multiplicar</option>
			  <option value="dividir">Dividir</option>
		</select>

		<?php

		if(isset($_SESSION['error2']))
		{
			echo "<font color='red'>". $_SESSION['error2']."</font>";
		}
		else
		{
			echo "";
		}
		unset($_SESSION['error2']);
		?>

		</p>

		<p>&nbsp;</p>

		<p>
		  <input type="submit" name="calcular" id="calcular" value="Calcular" />
		</p>
		</form>

	</center>
	</th>
	</tr>
	</table>
  </body>
</html>






