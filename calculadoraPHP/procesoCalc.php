<?php
session_start();

$num1=$_POST ['num1'];
$num2=$_POST ['num2'];
$operacion=$_POST ['operacion'];

$errores = array();

if($_POST["num1"] == "" || !is_numeric($num1)) 
{ 
	$errores[] = true;
	$_SESSION['error']= "Debe ingresar un número"; 
}

if($_POST["num2"] == "" || !is_numeric($num2))
{
	$errores[] = true;
	$_SESSION['error1']= "Debe ingresar un número";
}

if($_POST["operacion"] == "Seleccione una operación")
{
	$errores[] = true;
	$_SESSION['error2']= "Debe seleccionar un operación";
}
	
if (count( $errores)>0)
{
	header('location:calculadora.php');
}
else
{
	switch($operacion)
	{
		case "sumar":
			echo "El resultado de la suma de $num1 mas $num2 es: ";
			echo $num1 + $num2;
			break;
		case "restar":
			echo "El resultado de la resta de $num1 menos $num2 es: ";
			echo $num1 - $num2;
			break;
		case "multiplicar":
			echo "El resultado de la multiplicacion de $num1 por $num2 es: ";
			echo $num1 * $num2;
			break;
		case "dividir":
			if ($num2<>0)
			{
				echo "El resultado de la division de $num1 partido por $num2 es: ";
				echo $num1 / $num2;
			}
			else
			{
				$_SESSION['error1']= "Al dividir, el segundo número debe ser distinto de cero";
				header('location:calculadora.php');
			}			
			break;
		default:
			echo "Debe seleccionar una operacón";
			break;
	}
}

?>
